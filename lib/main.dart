import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';
import 'dart:io';
import 'dart:convert';

class Course {
  final String title;
  final String room;
  final String instructor;
  final int start;
  final int end;

  Course({this.title, this.room, this.instructor, this.start, this.end});

  factory Course.fromJson(Map<String, dynamic> json) {
    return Course(
      title: json['title'],
      room: json['room'],
      instructor: json['instructor'],
      start: json['start'],
      end: json['end'],
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'title': title,
      'room': room,
      'instructor': instructor,
      'start': start,
      'end': end,
    };
  }

  @override
  String toString() {
    return 'Room{title: $title, room: $room, instructor: $instructor, start: $start, end: $end';
  }
}

class Day {
  final String time;
  final String weekday;
  final List<Course> courses;

  Day({this.time, this.weekday, this.courses});
}

class Week {
  final String time;
  final List<Day> days;

  Week({this.time, this.days});
}

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        debugShowCheckedModeBanner: false, home: RoomsList()); // MaterialApp
  }
}

class RoomsList extends StatefulWidget {
  @override
  _RoomsListState createState() => new _RoomsListState();
}

class _RoomsListState extends State<RoomsList> {
  final storage = new FlutterSecureStorage();

  String userid;
  String hash;

//  var rooms = List<Room>();
  var weeksList = List<Week>();
  var isLoading = false;

  _getRooms() async {
    setState(() {
      isLoading = true;
    });

    final database =
        openDatabase(join(await getDatabasesPath(), 'rooms_database.db'),
            onCreate: (db, version) {
      return db.execute(
        "CREATE TABLE rooms(title TEXT, room TEXT, instructor TEXT, start INTEGER, end INTEGER)",
      );
    }, version: 1);

    final Database db = await database;

    var today = (new DateTime.now().millisecondsSinceEpoch) / 1000;

    final List<Map<String, dynamic>> maps =
        await db.query('rooms', where: '"start" >= ?', whereArgs: [today]);

    var weekdays = ['So', 'Mo', 'Di', 'Mi', 'Do', 'Fr', 'Sa'];

    var currentDay;
    var currentWeekFirstDay;
    var currentWeekLastDay;

    var courses = List<Course>();
    var days = List<Day>();
    var weeks = List<Week>();

    for (var courseJson in maps) {
      // course
      Course course = Course.fromJson(courseJson);
      // get time when course begins
      var courseStart =
          new DateTime.fromMillisecondsSinceEpoch(course.start * 1000);

      // its the first day in the list
      if (currentDay == null &&
          currentWeekFirstDay == null &&
          currentWeekLastDay == null) {
        currentWeekFirstDay =
            courseStart.subtract(new Duration(days: courseStart.weekday - 1));
        currentWeekLastDay = new DateTime(
                currentWeekFirstDay.year,
                currentWeekFirstDay.month,
                currentWeekFirstDay.day,
                23,
                59,
                59,
                999)
            .add(new Duration(days: 6));
        currentDay = courseStart.day;
        courses.add(course);
        continue;
      }

      if (courseStart.isAfter(currentWeekLastDay)) {
        weeks.add(Week(
            time:
                '${currentWeekFirstDay.day}.${currentWeekFirstDay.month} - ${currentWeekLastDay.day}.${currentWeekLastDay.month}',
            days: days));

        courses = List<Course>();
        days = List<Day>();

        currentWeekFirstDay =
            courseStart.subtract(new Duration(days: courseStart.weekday - 1));
        currentWeekLastDay = new DateTime(
                currentWeekFirstDay.year,
                currentWeekFirstDay.month,
                currentWeekFirstDay.day,
                23,
                59,
                59,
                999)
            .add(new Duration(days: 6));

        currentDay = courseStart.day;
      }

      if (currentDay != courseStart.day) {
        var lastDay =
            new DateTime.fromMillisecondsSinceEpoch(courses.last.start * 1000);
        days.add(Day(
            time: '${lastDay.day}.${lastDay.month}',
            weekday: weekdays[lastDay.weekday],
            courses: courses));

        currentDay = courseStart.day;

        courses = List<Course>();

        courses.add(course);
        continue;
      }

      courses.add(course);
    }

    setState(() {
      weeksList = weeks;
      isLoading = false;
    });
  }

  _loadRooms() async {
    final database =
        openDatabase(join(await getDatabasesPath(), 'rooms_database.db'),
            onCreate: (db, version) {
      return db.execute(
        "CREATE TABLE rooms(title TEXT, room TEXT, instructor TEXT, start INTEGER, end INTEGER)",
      );
    }, version: 1);

    userid = await storage.read(key: 'userid');
    hash = await storage.read(key: 'hash');

    var startDate = new DateTime.now();
    var endDate = startDate.add(new Duration(days: 100));

    var start = (startDate.millisecondsSinceEpoch / 1000).round();
    var end = (endDate.millisecondsSinceEpoch / 1000).round();

    Uri url = Uri.parse(
        'https://selfservice.campus-dual.de/room/json?userid=$userid&hash=$hash&start=$start&end=$end');

    HttpClient client = new HttpClient();
    client.badCertificateCallback =
        (X509Certificate cert, String host, int port) => true;

    HttpClientRequest request = await client.getUrl(url);
    HttpClientResponse response = await request.close();

    StringBuffer sb = new StringBuffer();
    await for (String data
        in response.transform(new AsciiDecoder(allowInvalid: true))) {
      sb.write(data);
    }

    var data = sb.toString();

    if (response.statusCode == 200) {
      try {
        Iterable list = json.decode(data);
        if (list.length == 0) return;
        final Database db = await database;

        await db.execute("DELETE FROM rooms");
        debugPrint('deleted all records from rooms');
        await db.execute("VACUUM");

        list.forEach((json) async {
          Course room = Course.fromJson(json);

          await db.insert(
            'rooms',
            room.toMap(),
            conflictAlgorithm: ConflictAlgorithm.replace,
          );
        });

        await _getRooms();
      } catch (e) {}
    }
  }

  @override
  void initState() {
    super.initState();
    _getRooms();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
          preferredSize: Size.fromHeight(56),
          child: Builder(
              builder: (scaffoldContext) => AppBar(
                    title: Text('BA App'),
                    actions: [
                      IconButton(
                          icon: Icon(Icons.autorenew),
                          onPressed: () async {
                            await _loadRooms();
                            final snackbar = SnackBar(
                              content: Text('Updated database.'),
                            );

                            Scaffold.of(scaffoldContext).showSnackBar(snackbar);
                          }),
                      IconButton(
                        icon: Icon(Icons.settings),
                        onPressed: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(builder: (context) => Settings()),
                          );
                        },
                      ),
                    ],
                  ))), // AppBar
      body: (isLoading
          ? Center(child: CircularProgressIndicator())
          : (weeksList.length > 0
              ? ListView.separated(
                  itemCount: weeksList.length,
                  separatorBuilder: (context, index) => Divider(),
                  itemBuilder: (context, index) {
//
                    return Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                          child: Text(weeksList[index].time),
                          alignment: Alignment(0.0, 0.0),
                          padding: new EdgeInsets.all(20.0),
                        ),
                        Column(
                          children: weeksList[index]
                              .days
                              .map((day) => Row(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Container(
                                          child: Column(
                                            children: [
                                              Text(day.weekday),
                                              Text(day.time,
                                                  style: TextStyle(
                                                      fontWeight:
                                                          FontWeight.bold))
                                            ],
                                          ),
                                          padding: new EdgeInsets.all(10.0),
                                          width: 60.0,
                                        ),
                                        Expanded(
                                            child: Container(
                                                child: Column(
                                          children: day.courses.map((room) {
                                            var start = new DateTime
                                                    .fromMillisecondsSinceEpoch(
                                                room.start * 1000);
                                            var end = new DateTime
                                                    .fromMillisecondsSinceEpoch(
                                                room.end * 1000);
                                            return Card(
                                              child: ListTile(
                                                  title: Text(room.title),
                                                  subtitle: Text(
                                                      '${start.hour}:${start.minute} - ${end.hour}:${end.minute} Raum: ${room.room}\n${room.instructor}')),
                                            );
                                          }).toList(),
                                        )))
                                      ]))
                              .toList(),
                        )
                      ],
                    );
                  })
              : Center(child: Text('Keine Termine gefunden.')))),
    ); // Scaffold
  }
}

class Settings extends StatefulWidget {
  @override
  _SettingsState createState() => new _SettingsState();
}

class _SettingsState extends State<Settings> {
  final storage = new FlutterSecureStorage();

  String userid;
  String hash;
  bool showHash = false;

  TextEditingController useridController = new TextEditingController();
  TextEditingController hashController = new TextEditingController();

  _readStorage() async {
    userid = await storage.read(key: 'userid');
    hash = await storage.read(key: 'hash');
    useridController.text = userid;
    hashController.text = hash;
  }

  _writeStorage(key, value) async {
    await storage.write(key: key, value: value);
  }

  @override
  void initState() {
    super.initState();
    _readStorage();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('BA App'),
      ), // AppBar
      body: Builder(
          builder: (scaffoldContext) => Padding(
                padding: EdgeInsets.all(20.0),
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      TextField(
                          decoration: InputDecoration(
                            labelText: 'UserId',
                          ),
                          controller: useridController,
                          keyboardType: TextInputType.number,
                          onChanged: (value) {
                            setState(() {
                              userid = value;
                            });
                          }),
                      TextField(
                          decoration: InputDecoration(
                            labelText: 'Hash',
                          ),
                          controller: hashController,
                          obscureText: !showHash,
                          onChanged: (value) {
                            setState(() {
                              hash = value;
                            });
                          }),
                      CheckboxListTile(
                          value: showHash,
                          title: Text('Hash anzeigen'),
                          controlAffinity: ListTileControlAffinity.leading,
                          onChanged: (bool value) {
                            setState(() {
                              showHash = !showHash;
                            });
                          }),
                      Container(
                          margin: const EdgeInsets.only(top: 10.0),
                          child: RaisedButton(
                            onPressed: () {
                              _writeStorage('userid', userid);
                              _writeStorage('hash', hash);
                              final snackbar = SnackBar(
                                content: Text('Saved userid and hash.'),
                              );

                              Scaffold.of(scaffoldContext)
                                  .showSnackBar(snackbar);
                            },
                            child: Text('Save'),
                          ))
                    ]),
              )),
    ); // Scaffold
  }
}
